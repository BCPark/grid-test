;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.config(["$stateProvider", "$urlRouterProvider",
		function ($stateProvider, $urlRouterProvider) {
			$stateProvider.state("main", {
				url: "/main",
				template: "<main></main>"
			});

			$stateProvider.state("page1", {
				url: "/page1",
				template: "<page1></page1>"
			});

			//--------------------------------------------------------------------------------

			$urlRouterProvider.otherwise("/main");
		}]);
})(window, _, jQuery, angular);
