;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.directive("mainColumnRenderer", function () {
		var scope = {gridOptions: "=", value:"@",fieldName: "@"};

		var templateUrl = "/apps/index/main/column-renderer/main-column-renderer.html";

		var controller = ["$scope", "$element", "reqToSvr", function ($scope, $element, reqToSvr) {
			$element.parent().parent().parent()[0].addEventListener('contextmenu', function (event) {
				$scope.gridOptions.api.showColumnMenuAfterMouseClick($scope.fieldName, event);
				event.preventDefault();
			});
		}];

		return {
			restrict: "E",
			scope: scope,
			templateUrl: templateUrl,
			replace: true,
			controller: controller
		};
	});

})(window, _, jQuery, angular);
