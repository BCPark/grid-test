;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.directive("mainColumnCheckbox", function () {
		var scope = {gridOptions: "=", value:"@",fieldName: "@"};

		var templateUrl = "/apps/index/main/column-checkbox/main-column-checkbox.html";

		var controller = ["$scope", "$element", "reqToSvr", function ($scope, $element, reqToSvr) {
			$element.parent().parent().parent()[0].addEventListener('contextmenu', function (event) {
				$scope.gridOptions.api.showColumnMenuAfterMouseClick($scope.fieldName, event);
				event.preventDefault();
			});

			$scope.toggle = function() {
				$scope.isChecked = !$scope.isChecked;
				var model = $scope.gridOptions.api.getModel();
				console.log("rowsToDisplay", model.rowsToDisplay);

				if($scope.isChecked) {
					_.forEach(model.rowsToDisplay,function(row) {
						row.selected = true;
					})
				}
				else {
					_.forEach(model.rowsToDisplay,function(row) {
						row.selected = false;
					})
				}
			};
		}];

		return {
			restrict: "E",
			scope: scope,
			templateUrl: templateUrl,
			replace: true,
			controller: controller
		};
	});

})(window, _, jQuery, angular);
