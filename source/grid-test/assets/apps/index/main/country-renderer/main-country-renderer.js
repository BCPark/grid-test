;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.directive("mainCountryRenderer", function () {
		var scope = { name:"@"};

		var templateUrl = "/apps/index/main/country-renderer/main-country-renderer.html";

		var FLAG_CODES = {
			'Ireland': 'ie',
			'United States': 'us',
			'Russia': 'ru',
			'Australia': 'au',
			'Canada': 'ca',
			'Norway': 'no',
			'China': 'cn',
			'Zimbabwe': 'zw',
			'Netherlands': 'nl',
			'South Korea': 'kr',
			'Croatia': 'hr',
			'France': 'fr'
		};

		var controller = ["$scope", "$element", "reqToSvr", function ($scope, $element, reqToSvr) {
			$scope.imgPath = FLAG_CODES[$scope.name] ? "/images/index/"+FLAG_CODES[$scope.name]+".png" : null;
		}];

		return {
			restrict: "E",
			scope: scope,
			templateUrl: templateUrl,
			replace: true,
			controller: controller
		};
	});

})(window, _, jQuery, angular);
