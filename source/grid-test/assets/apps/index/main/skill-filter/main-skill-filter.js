;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.directive("mainSkillFilter", function () {
		var scope = { skills : "=", title : "@", onClick : "&"};

		var templateUrl = "/apps/index/main/skill-filter/main-skill-filter.html";

		var controller = ["$scope", "$element", "reqToSvr", function ($scope, $element, reqToSvr) {

			$scope.getIamgePath = function(imageName) {
				return "/images/index/"+imageName+".png";
			};

		}];

		return {
			restrict: "E",
			scope: scope,
			templateUrl: templateUrl,
			replace: true,
			controller: controller
		};
	});

})(window, _, jQuery, angular);
